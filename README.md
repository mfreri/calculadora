# Calculadora

A simple calculator made in Python and GTK.

It only support basic operations and may be buggy as hell, but it's not intended for daily use.

---

Una calculadora sencilla hecha en Python y GTK.

Realiza solamente las operaciones básicas y debe de estar plagada de errores, pero la idea nunca fue utilizarla en producción.
