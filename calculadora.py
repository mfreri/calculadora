#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Calculadora de ejemplo con GTK+ y Glade.
# Funciona con Python3 y GKT+ 3.0

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

LARGO_MAX = 8 # Máxima cantidad de dígitos


class Handler:
	def onDestroy(self, *args):
		Gtk.main_quit()

	def onAcerca(self, bAcerca):
		print("Calculadora 0.1 - Ejemplo de GTK en Python3.")
		print("(C) Marcelo Freri, 2020 - Licencia GNU GPL versión 3.")

	def onPanico(self, bPanico):
		print("Ha ocurrido un problema y se limpiaron los datos.")
		self.onAllclear(bPanico)

	def onPulsa(self, bPulsado): # Cuando pulsa un dígito o el punto decimal
		valor = bPulsado.get_label()
		etiqueta = builder.get_object("lValor")
		global borro_etiqueta, decimal
		if valor != "0":
			if borro_etiqueta:
				if valor == "." and not decimal:
					etiqueta.set_label("0.")
					decimal = True
				else:
					etiqueta.set_label(valor)
				borro_etiqueta = False
			else:
				if len(etiqueta.get_label()) < LARGO_MAX:
					if valor == ".":
						if not decimal:
							etiqueta.set_label(etiqueta.get_label() + valor)
							decimal = True
					else:
						etiqueta.set_label(etiqueta.get_label() + valor)
					borro_etiqueta = False
		else:
			if not borro_etiqueta and len(etiqueta.get_label()) < LARGO_MAX:
				etiqueta.set_label(etiqueta.get_label() + valor)

	def borra_etiqueta(self):
		global borro_etiqueta, decimal
		builder.get_object("lValor").set_label("0.")
		borro_etiqueta = True
		decimal = False

	def onOperacion(self, bOperacion):
		valor = builder.get_object("lValor").get_label()
		operando = bOperacion.get_label()
		global borro_etiqueta, decimal
		operacion.append([valor, operandos[operando]])
		builder.get_object("lOperacion").set_label(operandos[operando])
		borro_etiqueta = True
		decimal = False

	def onAllclear(self, bAC):
		global decimal
		self.borra_etiqueta()
		builder.get_object("lOperacion").set_label(" ")
		operacion.clear()

	def onClear(self, bClear):
		self.borra_etiqueta()

	def onIgual(self, bIgual):
		valor = builder.get_object("lValor").get_label()
		largo_lista = len(operacion)
		global borro_etiqueta, decimal
		resultado = 0
		if largo_lista > 0:
			operacion.append([valor, "="])
			largo_lista = len(operacion)
			for fila in range(0, largo_lista):
				if fila == 0: # Primer valor ingresado
					resultado = float(operacion[0][0])
				else: # No es el primer valor ingresado
					if operacion[fila - 1][1] == "+":
						resultado = resultado + float(operacion[fila][0])
					elif operacion[fila - 1][1] == "-":
						resultado = resultado - float(operacion[fila][0])
					elif operacion[fila - 1][1] == "x":
						resultado = resultado * float(operacion[fila][0])
					elif operacion[fila - 1][1] == "/":
						if float(operacion[fila][0]) == 0:
							print("[E] Divide by zero.")
							resultado = "error"
						else:
							resultado = resultado / float(operacion[fila][0])
		borro_etiqueta = True
		builder.get_object("lValor").set_label(str(resultado)) # Muestra el resultado
		builder.get_object("lOperacion").set_label("=") # Muestra la operación realizada
		operacion.clear()


operacion = [[]]
operacion.clear()
operandos = {
	"➕": "+",
	"➖": "-",
	"❌": "x",
	"➗": "/"
}
borro_etiqueta = True
decimal = False

# Glade canvas
builder = Gtk.Builder()
builder.add_from_file("calculadora.glade")
builder.connect_signals(Handler())

# Main
ventana_principal = builder.get_object("wPrinc")
ventana_principal.show_all()

Gtk.main()